package com.jabi.api.constants;

public class BaseURL {

	
	public static final String MOOSEND 		= "https://api.moosend.com/v3/";
	public static final String SEND_FOX 	= "https://api.sendfox.com/";
	public static final String MAILER_LIGHT = "https://api.mailerlite.com/api/v2/";

	
}
