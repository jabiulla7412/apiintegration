package com.jabi.api.constants;

public class HttpHeaders {
 
	public static final String KEY_CONTENT_TYPE = "Content-Type:";
	public static final String KEY_ACCEPT		= "Accept:";
 	public static final String VALUE_APPLICATION_JSON   ="application/json";
 	
	public static final String KEY_AUTHORIZATION = "Authorization";

  
}
