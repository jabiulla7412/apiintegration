package com.jabi.api.constants;

public class HttpMethods {
	
	public static final String POST 	= "post";
	public static final String GET 		= "get";
	public static final String PUT	    = "put";
	public static final String DELETE 	= "delete";

}
