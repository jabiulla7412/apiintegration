package com.jabi.api;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.jabi.api.constants.HttpMethods;
import com.jabi.api.exception.ApiException;
import com.jabi.api.exception.URLException;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.internal.http.HttpMethod;

public abstract class HttpConnection {
	
	private static Logger logger = Logger.getLogger(HttpConnection.class);

	private OkHttpClient client  = null;
	
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
 
    public abstract String     getHttpMethod();
	public abstract String     getURL() throws Exception;
	public abstract HashMap<String,String> getHeaders();
	public abstract JSONObject getRequest() throws Exception;
	public abstract void       setResponse(String response) throws Exception;
	//public abstract String     getResponse() throws Exception;
 
	public void open() {
		  client = new OkHttpClient();
	}
	
	public void close() {
		//
	}
	
    private String doGetRequest(String url) throws IOException {
        Headers headerbuild = Headers.of(getHeaders());

       Request request = new Request.Builder()
    	   .headers(headerbuild)
           .url(url)
           .build();

       Response response = client.newCall(request).execute();
       return response.body().string();
   }

   // post request code here
   private String doPostRequest(String url, String json) throws IOException {
         RequestBody body = RequestBody.create(JSON, json);
         Headers headerbuild = Headers.of(getHeaders());
            Request request = new Request.Builder()
        	   .headers(headerbuild)
               .url(url)
               .post(body)
               .build();
           Response response = client.newCall(request).execute();
           return response.body().string();
   }
   
   public void printLog() throws Exception {
	   logger.info("---------------REQUEST FORMAT--------------");
	   logger.info("URL  : " + getURL());
	   logger.info("HTTP METHOD " + getHttpMethod());
	   logger.info("HTTP request " + getRequest());
   }
   
   public void execute() {
	   
	   try {
		   
		   open();
		   
		   printLog();
		   
		   if( getURL() == null ) throw new URLException("Invalid URL");
		   
		   if(getHeaders() == null ) throw new ApiException("Invalid Headers");
		    
		   if(HttpMethods.POST.equals(getHttpMethod())){
			   if(getRequest() == null ) throw new ApiException("No Request found , for POST request");
	 			setResponse(doPostRequest(getURL(),getRequest().toString()));
	 		}else if(HttpMethods.GET.equals(getHttpMethod())) {
	 	 		   setResponse(doGetRequest(getURL()));
	        }else {
	 		   
	        }
		   
		} catch (ApiException e) {
 			e.printStackTrace();
 			logger.error("Exception : "+ e.fillInStackTrace() + "\n" + e.getMessage() );
		} catch (URLException e) {
 			//e.printStackTrace();
 			logger.error("Exception : " + e.fillInStackTrace() + e.getMessage() );

		} catch (IOException e) {
 			e.printStackTrace();
 			logger.error("Exception : " + e.fillInStackTrace() + e.getMessage() );

		} catch (Exception e) {
 			e.printStackTrace();
 			logger.error("Exception : " + e.fillInStackTrace() + e.getMessage() );
		}
	 
   }
   
 

}
