package com.jabi.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.jabi.api.mailerlight.FetchAllSubscribersFromMAilerLight;
import com.jabi.api.sendfox.CheckContactInSendFoxAPI;
import com.jabi.api.sendfox.CreateContactInSendFoxAPI;

 
@SpringBootApplication
public class ApiIntegrationApplication {

	private static Logger logger = Logger.getLogger(ApiIntegrationApplication.class);

	public static void main(String[] args) throws Exception {
		 SpringApplication.run(ApiIntegrationApplication.class, args);
	  
 		new ApiIntegrationApplication().integrateMailerLiteAndSendFox();
		  
	}
	
	private List<String> dataList = new ArrayList<>();
	
	private void integrateMailerLiteAndSendFox() {
		
		while(true) {
			logger.info("-------------Integration MAilerLite And SendFox-----------");
			logger.info("Press 1 To Fetch All Subscribers From Mailer Lite ");
			logger.info("Press 2 Mailer Lite Not Present In SendFox \n Create Contact in SendFox ");
			logger.info("Press 3 For Exit");

			Scanner scan = new Scanner(System.in);
			int choice = scan.nextInt();
			
			switch (choice) {
			
			case 1 :    FetchAllSubscribersFromMAilerLight fetch = new FetchAllSubscribersFromMAilerLight();
						fetch.execute();
						dataList = fetch.getList();
  					    break;
   					    
			case 2 : 	
				        for(String email : dataList) {
				        	CheckContactInSendFoxAPI check = new CheckContactInSendFoxAPI();
				        	check.setEmailId(email);
				            check.execute();
				            
				            if(check.contactAvailableOrNot()) {
				            	CreateContactInSendFoxAPI create = new CreateContactInSendFoxAPI();
				            	create.setEmailId(email);
				            	create.execute();
				            }
				        }
   			        break;
				
		    default:logger.error("Invalid Choice");		
					break;
			}

			
		}
	}
}
