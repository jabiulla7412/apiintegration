package com.jabi.api.sendfox;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.jabi.api.HttpConnection;
import com.jabi.api.constants.BaseURL;
import com.jabi.api.constants.Credentials;
import com.jabi.api.constants.HttpHeaders;
import com.jabi.api.constants.HttpMethods;
import com.jabi.api.mailerlight.FetchAllSubscribersFromMAilerLight;

public class CreateContactInSendFoxAPI extends HttpConnection{

	private static Logger logger = Logger.getLogger(CreateContactInSendFoxAPI.class);

	private final String contactURL = "contact";
	
	private String emailId;
	
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	
	@Override
	public String getHttpMethod() {
 		return HttpMethods.POST;
	}

	@Override
	public String getURL() throws Exception {
 		return BaseURL.SEND_FOX + contactURL;
	}

	@Override
	public HashMap<String, String> getHeaders() {
		HashMap<String, String> headers = new HashMap<String, String> ();
		headers.put(HttpHeaders.KEY_ACCEPT, HttpHeaders.VALUE_APPLICATION_JSON);
		headers.put(HttpHeaders.KEY_AUTHORIZATION, "Bearer " + Credentials.SEND_FOX_ACCESS_TOKEN);
 		return headers;
	}

	@Override
	public JSONObject getRequest() throws Exception {
		JSONObject jsonRequest = new JSONObject();
		jsonRequest.put("email", getEmailId());
		return jsonRequest;
	}

	@Override
	public void setResponse(String response) throws Exception {
 		logger.info("Response is " + response);
	}
 

}
