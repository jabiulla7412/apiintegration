package com.jabi.api.sendfox;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.jabi.api.HttpConnection;
import com.jabi.api.constants.BaseURL;
import com.jabi.api.constants.Credentials;
import com.jabi.api.constants.HttpHeaders;
import com.jabi.api.constants.HttpMethods;

public class CheckContactInSendFoxAPI extends HttpConnection{

	private static Logger logger = Logger.getLogger(CheckContactInSendFoxAPI.class);
	
	private String checkContactURL = "contacts?email=";
	
	private String emailId;
	
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@Override
	public String getHttpMethod() {
 		return HttpMethods.GET;
	}

	@Override
	public String getURL() throws Exception {
 		return BaseURL.SEND_FOX + checkContactURL + getEmailId() ;
	}

	@Override
	public HashMap<String, String> getHeaders() {
		HashMap<String, String> headers = new HashMap<String, String> ();
		headers.put(HttpHeaders.KEY_ACCEPT, HttpHeaders.VALUE_APPLICATION_JSON);
		headers.put(HttpHeaders.KEY_AUTHORIZATION, "Bearer " + Credentials.SEND_FOX_ACCESS_TOKEN);
 		return headers;
	}

	@Override
	public JSONObject getRequest() throws Exception {
  		return null;
	}
	
	private boolean contactAvailable = false ; 

	@Override
	public void setResponse(String response) throws Exception {
		logger.info(response);
		
		JSONObject json = new JSONObject(response);
 		int contactNotFound = json.getJSONArray("data").length();
 		if(contactNotFound == 0) contactAvailable = true;
	}
	
	public boolean contactAvailableOrNot() {
		return contactAvailable;
	}

 

}
