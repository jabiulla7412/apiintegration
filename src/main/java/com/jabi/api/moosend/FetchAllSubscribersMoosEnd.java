package com.jabi.api.moosend;

import java.util.HashMap;

import org.json.JSONObject;

import com.jabi.api.HttpConnection;
import com.jabi.api.constants.BaseURL;
import com.jabi.api.constants.Credentials;
import com.jabi.api.constants.HttpHeaders;
import com.jabi.api.constants.HttpMethods;

public class FetchAllSubscribersMoosEnd extends HttpConnection {
 
	private final String fetchSubscriberUrl = "lists/a589366a-1a34-4965-ac50-f1299fe5979e/subscribers/Subscribed.json?apikey=";
	
	@Override
	public String getHttpMethod() {
		return HttpMethods.GET ;
	}

	@Override
	public String getURL() throws Exception {
 		return BaseURL.MOOSEND + fetchSubscriberUrl + Credentials.MOOSEND_API_KEY;
	}

	@Override
	public HashMap<String, String> getHeaders() {
		 HashMap<String, String>  headers = new  HashMap<String, String>();
		 headers.put(HttpHeaders.KEY_CONTENT_TYPE,HttpHeaders.VALUE_APPLICATION_JSON);
		 headers.put(HttpHeaders.KEY_ACCEPT, HttpHeaders.VALUE_APPLICATION_JSON);
		 return headers;
	}

	@Override
	public JSONObject getRequest() throws Exception {
		return null;
	}
	
	@Override
	public void setResponse(String response) throws Exception {

	}


}
