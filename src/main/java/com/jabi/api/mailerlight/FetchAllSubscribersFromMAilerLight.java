package com.jabi.api.mailerlight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.jabi.api.ApiIntegrationApplication;
import com.jabi.api.HttpConnection;
import com.jabi.api.constants.BaseURL;
import com.jabi.api.constants.Credentials;
import com.jabi.api.constants.HttpHeaders;
import com.jabi.api.constants.HttpMethods;

public class FetchAllSubscribersFromMAilerLight extends HttpConnection {
  
	private static Logger logger = Logger.getLogger(FetchAllSubscribersFromMAilerLight.class);

	private String fetchAllSubscribersURL ="subscribers";
	 
	@Override
	public String getHttpMethod() {
 		return HttpMethods.GET;
	}

	@Override
	public String getURL() throws Exception {
 		return BaseURL.MAILER_LIGHT + fetchAllSubscribersURL ;
	}

	@Override
	public HashMap<String, String> getHeaders() {	
		 Map<String ,String > headers = new HashMap<String,String>();
		 headers.put("X-MailerLite-ApiKey", Credentials.MAILER_LITE_API_KEY );
		 return  (HashMap<String, String>) headers;
	}

	@Override
	public JSONObject getRequest() throws Exception {
		return null;
	}
 
	private List<String> mailerLiteContactsList = null ;
	
	@Override
	public void setResponse(String response) throws Exception {
	    logger.info("HTTP Response : " + response  );
 	    //parse response 
	    JSONArray jsonArray = new JSONArray(response);
	    mailerLiteContactsList = new ArrayList<>();
 	    for(int index = 0 ; index < jsonArray.length() ; index++) {
	    	logger.info(jsonArray.get(index));
	    	String email = jsonArray.getJSONObject(index).getString("email");
	    	mailerLiteContactsList.add(email);
  	    }
    }
	
	public List<String> getList(){
 		return mailerLiteContactsList;
	}

}
