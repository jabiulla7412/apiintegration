package com.jabi.api.exception;

public class ApiException extends Exception {

	private String mMessage;
	 
	public ApiException(String message){
		super(message);
		mMessage = message ;
	}
	
	public String getMessage() {
		return mMessage;
	}
	 
}
