package com.jabi.api.exception;

public class URLException  extends Exception {

	private String mMessage;
	 
	public URLException(String message){
		super(message);
		mMessage = message ;
	}
	
	public String getMessage() {
		return mMessage;
	}
	 
}
